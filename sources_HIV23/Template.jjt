options {
	JAVA_UNICODE_ESCAPE=true;
	STATIC=false;					// 1 seul parseur actif a la fois (+ rapide)
	MULTI=true;						// type noeud = f(nom noeud)
	VISITOR=true;					// Cree une methode accept(visiteur)
}

//
// PARSER DEFINITION
//

PARSER_BEGIN(Parser)

package analyzer.ast;

public class Parser
{
	public static ASTProgram ParseTree(java.io.InputStream input) throws ParseException
	{
		Parser c = new Parser(input);
		return c.Program();
	}
}

PARSER_END(Parser)

//
// LEXICAL ANALYSIS
//

// White space

SKIP :
{
	< ( " " | "\t" | "\n" | "\r" )+	>
}

// Comments

MORE :
{
  "//" : IN_SINGLE_LINE_COMMENT
|
  <"/**" ~["/"]> { input_stream.backup(1); } : IN_FORMAL_COMMENT
|
  "/*" : IN_MULTI_LINE_COMMENT
}

<IN_SINGLE_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <SINGLE_LINE_COMMENT: "\n" | "\r" | "\r\n" > : DEFAULT
}

<IN_FORMAL_COMMENT>
SPECIAL_TOKEN :
{
  <FORMAL_COMMENT: "*/" > : DEFAULT
}

<IN_MULTI_LINE_COMMENT>
SPECIAL_TOKEN :
{
  <MULTI_LINE_COMMENT: "*/" > : DEFAULT
}

<IN_SINGLE_LINE_COMMENT,IN_FORMAL_COMMENT,IN_MULTI_LINE_COMMENT>
MORE :
{
  < ~[] >
}




// Keywords
TOKEN :
{
	< IF: "if"	> |
	< ELSE: "else" > |
	< STRING: "string"> |
	< INT: "int"> |
	< FLOAT: "float" > |
	< BOOL: "bool"> |
	< PRINT: "print" > |
	< INPUT: "input" > |
	< IN: "in" > |
	< DO: "do" > |
	< WHILE: "while" > |
	< SWITCH: "switch" > |
	< CASE: "case" > |
    < DEF: "default">|
    < FOR: "for"> |
    < FUNC: "func"> |
    < RET: "return"> |
    < TRUE: "true" > |
    < FALSE: "false" >
	// À compléter au besoin
}

// Operators
TOKEN :
{
	< ASSIGN: < EQUAL >  > |
	< COMPARE: < LESS > | < LESSEQUAL > | < GREAT > | <GREATEQUAL > | < DIFF > | < EQUALEQUAL > > |
    < LOGIC: < AND > | < OR > > |
	< #LESS: "<" > |
	< #LESSEQUAL: "<=" > |
	< #GREAT: ">" > |
	< #GREATEQUAL: ">=" > |
	< #DIFF: "!=" > |
	< #EQUALEQUAL: "==" > |

	< PLUS: "+" > |
	< MINUS: "-" > |
	< FOIS: "*" > |
	< DIV: "/" > |
    < NOT: "!" > |

	< #EQUAL: "=" > |
	< LPAREN: "(" > |
	< RPAREN: ")" > |
	< LACC: "{" > |
	< RACC: "}" > |
	< COLON: ":" > |
	< SEMICOLON: ";" > |
	< COMMA: "," > |
	< #AND: "&&" > |
	< #OR: "||" >
	// À compléter au besoin
}


// Identifiers and numbers
TOKEN :
{
	< IDENTIFIER: <LETTER> ( <LETTER> | <DIGIT> )* > |
	< #LETTER: ["a"-"z","A"-"Z","_"] > |
	< #DIGIT: ["0"-"9"] > |
	< INTEGER: ["1"-"9"] (["0"-"9"])* | "0" > |
	< BOOLEAN: "true" | "false"> |
	< EXP: "e+" | "E+"> |
	//2.5 Les nombres réels
	< REAL: (<INTEGER>)? "." (<INTEGER>)? (<EXP> <INTEGER> )? > //TODO
}

//
// SYNTAX ANALYSIS
//

ASTProgram Program() : { }
{
	Block() <EOF> { return jjtThis;  }
}

void Block() #void : { }
{
	 ( Stmt() )*
}

void Stmt() #void : { }
{
	AssignStmt() <SEMICOLON> | WhileStmt() | DoWhileStmt() | IfStmt() | ForStmt() | FunctionStmt()
}


void AssignStmt() : { Token t;}
{
	Identifier() <ASSIGN> Expr()
}

void Expr() #void : {}
{
    BasicExpr()
}

void BasicExpr() #void : {}
{
	LogicExpr()
}

void Terminal() #void : {}
{
    LOOKAHEAD(2) Identifier() | IntValue() | RealValue() | BoolValue()
    | LOOKAHEAD(2) <LPAREN> Expr() <RPAREN>
}

void Identifier() : { Token t;}
{
	t = <IDENTIFIER> {jjtThis.setValue(t.image);}
}

void BoolValue() : {}
{
	<BOOL> <BOOLEAN>
}

void IntValue() : { Token t;}
{
	t = <INTEGER> {jjtThis.setValue(Integer.parseInt(t.image));}
}

//2.5 Les nombres réels
void RealValue() : { Token t;}
{
	t = <REAL> {jjtThis.setValue(Double.parseDouble(t.image));}
}

//2.1 Les boucles while et do-while

void WhileCond() : {}
{
    Expr()
}

void WhileBlock(): {}
{
    <LACC> Block() [ReturnStmt()] <RACC> | Stmt()
}

//Listing 1 Boucle While
void WhileStmt() : {}
{
    <WHILE> <LPAREN> WhileCond() <RPAREN> WhileBlock()
}

//Listing 2 Boucle Do-While
void DoWhileStmt() : {}
{
    <DO> WhileBlock() <WHILE> <LPAREN> WhileCond() <RPAREN> <SEMICOLON>
}


//2.2 Les structures conditionnelles

void IfCond() : {}
{
    Expr()
}

void IfBlock() : {}
{
    <LACC> Block() [ReturnStmt()]<RACC> | Stmt()
}

void ElseBlock() : {}
{
    LOOKAHEAD(2) IfStmt() | (<LACC> Block() [ReturnStmt()]<RACC> | Stmt())
}

//Listing 3-6 toutes les boucles if/else
void IfStmt() : {}
{
    <IF> <LPAREN> IfCond() <RPAREN>
    IfBlock()
    (LOOKAHEAD(2) <ELSE> ElseBlock())*
}

//2.3 La structure for

//Listing 7 For
void ForStmt() : {}
{
    <FOR> <LPAREN> [AssignStmt()] <SEMICOLON> [Expr()] <SEMICOLON> [AssignStmt()] <RPAREN>
    (<LACC> Block() [ReturnStmt()]<RACC> | Stmt())
}


//2.4 Priorité des opérations

void LogicExpr() #void : {}
{
    (CompExpr() ((LOOKAHEAD(2) <LOGIC> CompExpr())*)) #Logic (>1)
}

void CompExpr() #void : {}
{
    (AddExpr() ((LOOKAHEAD(2) <COMPARE> AddExpr())*)) #Compare (>1)
}

void AddExpr() #void : {}
{
    (MulExpr() ((LOOKAHEAD(2) (<PLUS> | <MINUS>) MulExpr())*)) #Addition (>1)
}

void MulExpr() #void : {}
{
    (MinusExpr() ((LOOKAHEAD(2) (<FOIS> | <DIV>) MinusExpr())*)) #Multi (>1)
}

void MinusExpr() #void : {}
{
    ((<MINUS>) MinusExpr()) #Minus | NotExpr()
}

void NotExpr() #void : {}
{
    ((<NOT>) NotExpr()) #Not | Terminal()
}

//2.6 La structure fonction

void FunctionStmt() : {}
{
    <FUNC> Identifier() <LPAREN> [Identifier()] (<COMMA> Identifier())* <RPAREN> <LACC>
    Block() [ReturnStmt()]
    <RACC>
}

void ReturnStmt() : {}
{
    <RET> [Expr()] <SEMICOLON>
}